package com.boeing.distancecalculator.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Distance {
    private BigDecimal value;
    private LengthUnit unit;

    public Distance(BigDecimal value, LengthUnit unit) {
        this.value = value;
        this.unit = unit;
    }

    public BigDecimal getValue() {
        return value;
    }

    public LengthUnit getUnit() {
        return unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Distance distance = (Distance) o;
        return Objects.equals(value, distance.value) &&
                unit == distance.unit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, unit);
    }
}
