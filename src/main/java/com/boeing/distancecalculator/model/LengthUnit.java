package com.boeing.distancecalculator.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public enum LengthUnit {
    METER(1.0, "m"),
    FOOT(0.3048, "ft"),
    NAUTICAL_MILE(1852.0, "NM");

    private final double meters;
    private final String symbol;

    private static final Map<String, LengthUnit> symbols = new HashMap<>();
    static {
        for (LengthUnit unit : LengthUnit.values()) {
            symbols.put(unit.symbol, unit);
        }
    }

    LengthUnit(double meters, String symbol) {
        this.meters = meters;
        this.symbol = symbol;
    }

    public BigDecimal convertFrom(BigDecimal value, LengthUnit unit) {
        return BigDecimal.valueOf(value.doubleValue() * unit.meters / this.meters);
    }

    public BigDecimal convertFrom(Distance distance) {
        return convertFrom(distance.getValue(), distance.getUnit());
    }
}
