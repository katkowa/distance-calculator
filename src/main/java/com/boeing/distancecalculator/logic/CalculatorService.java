package com.boeing.distancecalculator.logic;

import com.boeing.distancecalculator.model.Distance;
import com.boeing.distancecalculator.model.LengthUnit;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CalculatorService {
    private final Calculator calculator = new Calculator();

    public Distance add(BigDecimal firstValue, LengthUnit firstUnit, BigDecimal secondValue, LengthUnit secondUnit, LengthUnit unit) {
        return calculator.add(new Distance(firstValue, firstUnit), new Distance(secondValue, secondUnit), unit);
    }

    public Distance subtract(BigDecimal firstValue, LengthUnit firstUnit, BigDecimal secondValue, LengthUnit secondUnit, LengthUnit unit) {
        return calculator.subtract(new Distance(firstValue, firstUnit), new Distance(secondValue, secondUnit), unit);
    }

    public Distance divide(BigDecimal firstValue, LengthUnit firstUnit, BigDecimal secondValue, LengthUnit secondUnit, LengthUnit unit) {
        return calculator.divide(new Distance(firstValue, firstUnit), new Distance(secondValue, secondUnit), unit);
    }

    public Distance multiply(BigDecimal firstValue, LengthUnit firstUnit, BigDecimal secondValue, LengthUnit secondUnit, LengthUnit unit) {
        return calculator.multiply(new Distance(firstValue, firstUnit), new Distance(secondValue, secondUnit), unit);
    }
}
