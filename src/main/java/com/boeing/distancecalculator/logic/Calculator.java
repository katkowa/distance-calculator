package com.boeing.distancecalculator.logic;

import com.boeing.distancecalculator.model.Distance;
import com.boeing.distancecalculator.model.LengthUnit;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

class Calculator {
    private final DecimalFormat format;

    Calculator() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        format = new DecimalFormat("#0.00", otherSymbols);
    }

    Distance add(Distance firstDistance, Distance secondDistance, LengthUnit resultUnit) {
        BigDecimal value = resultUnit.convertFrom(firstDistance).add(resultUnit.convertFrom(secondDistance));
        return new Distance(getFormatValue(value), resultUnit);
    }

    Distance subtract(Distance firstDistance, Distance secondDistance, LengthUnit resultUnit) {
        BigDecimal value = resultUnit.convertFrom(firstDistance) .subtract(resultUnit.convertFrom(secondDistance));
        return new Distance(getFormatValue(value), resultUnit);
    }

    Distance divide(Distance firstDistance, Distance secondDistance, LengthUnit resultUnit) {
        BigDecimal value = resultUnit.convertFrom(firstDistance).divide(resultUnit.convertFrom(secondDistance), RoundingMode.HALF_UP);
        return new Distance(getFormatValue(value), resultUnit);
    }

    Distance multiply(Distance firstDistance, Distance secondDistance, LengthUnit resultUnit) {
        BigDecimal value = resultUnit.convertFrom(firstDistance).multiply(resultUnit.convertFrom(secondDistance));
        return new Distance(getFormatValue(value), resultUnit);
    }

    private BigDecimal getFormatValue(BigDecimal value) {
        return new BigDecimal(format.format(value));
    }
}
