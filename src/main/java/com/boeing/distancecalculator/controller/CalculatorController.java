package com.boeing.distancecalculator.controller;

import com.boeing.distancecalculator.logic.CalculatorService;
import com.boeing.distancecalculator.model.Distance;
import com.boeing.distancecalculator.model.LengthUnit;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class CalculatorController {
    private final CalculatorService calculatorService;

    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @GetMapping("/add")
    public Distance add(@RequestParam BigDecimal firstValue, @RequestParam LengthUnit firstUnit,
                        @RequestParam BigDecimal secondValue, @RequestParam LengthUnit secondUnit,
                        @RequestParam LengthUnit unit) {
        return calculatorService.add(firstValue, firstUnit, secondValue, secondUnit, unit);
    }

    @GetMapping("/subtract")
    public Distance subtract(@RequestParam BigDecimal firstValue, @RequestParam LengthUnit firstUnit,
                        @RequestParam BigDecimal secondValue, @RequestParam LengthUnit secondUnit,
                        @RequestParam LengthUnit unit) {
        return calculatorService.subtract(firstValue, firstUnit, secondValue, secondUnit, unit);
    }

    @GetMapping("/divide")
    public Distance divide(@RequestParam BigDecimal firstValue, @RequestParam LengthUnit firstUnit,
                        @RequestParam BigDecimal secondValue, @RequestParam LengthUnit secondUnit,
                        @RequestParam LengthUnit unit) {
        return calculatorService.divide(firstValue, firstUnit, secondValue, secondUnit, unit);
    }

    @GetMapping("/multiply")
    public Distance multiply(@RequestParam BigDecimal firstValue, @RequestParam LengthUnit firstUnit,
                        @RequestParam BigDecimal secondValue, @RequestParam LengthUnit secondUnit,
                        @RequestParam LengthUnit unit) {
        return calculatorService.multiply(firstValue, firstUnit, secondValue, secondUnit, unit);
    }
}
