package com.boeing.distancecalculator.unit;

import com.boeing.distancecalculator.model.LengthUnit;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.boeing.distancecalculator.model.LengthUnit.*;
import static org.junit.jupiter.api.Assertions.*;

class LengthConversionTest {

    private final double EPSILON = 0.005;

    @Test
    void meterToMeterConversionTest() {
        assertEquals(0, METER.convertFrom(new BigDecimal(0), LengthUnit.METER).doubleValue(), EPSILON);
        assertEquals(1000, METER.convertFrom(new BigDecimal(1000), LengthUnit.METER).doubleValue(), EPSILON);
    }

    @Test
    void meterToFootConversionTest() {
        assertEquals(0, FOOT.convertFrom(new BigDecimal(0), LengthUnit.METER).doubleValue(), EPSILON);
        assertEquals(3280.84, FOOT.convertFrom(new BigDecimal(1000), LengthUnit.METER).doubleValue(), EPSILON);
    }

    @Test
    void meterToNauticalMileConversionTest() {
        assertEquals(0, NAUTICAL_MILE.convertFrom(new BigDecimal(0), LengthUnit.METER).doubleValue(), EPSILON);
        assertEquals(0.54, NAUTICAL_MILE.convertFrom(new BigDecimal(1000), LengthUnit.METER).doubleValue(), EPSILON);
    }

    @Test
    void footToFootConversionTest() {
        assertEquals(0, FOOT.convertFrom(new BigDecimal(0), FOOT).doubleValue(), EPSILON);
        assertEquals(1000, FOOT.convertFrom(new BigDecimal(1000), FOOT).doubleValue(), EPSILON);
    }

    @Test
    void footToMeterConversionTest() {
        assertEquals(0, METER.convertFrom(new BigDecimal(0), FOOT).doubleValue(), EPSILON);
        assertEquals(304.8, METER.convertFrom(new BigDecimal(1000), FOOT).doubleValue(), EPSILON);
    }

    @Test
    void footToNauticalMileConversionTest() {
        assertEquals(0, NAUTICAL_MILE.convertFrom(new BigDecimal(0), FOOT).doubleValue(), EPSILON);
        assertEquals(0.16, NAUTICAL_MILE.convertFrom(new BigDecimal(1000), FOOT).doubleValue(), EPSILON);
    }

    @Test
    void nauticalMileToNauticalMileConversionTest() {
        assertEquals(0, NAUTICAL_MILE.convertFrom(new BigDecimal(0), NAUTICAL_MILE).doubleValue(), EPSILON);
        assertEquals(1000, NAUTICAL_MILE.convertFrom(new BigDecimal(1000), NAUTICAL_MILE).doubleValue(), EPSILON);
    }

    @Test
    void nauticalMileToMeterConversionTest() {
        assertEquals(0, METER.convertFrom(new BigDecimal(0), NAUTICAL_MILE).doubleValue(), EPSILON);
        assertEquals(1852000, METER.convertFrom(new BigDecimal(1000), NAUTICAL_MILE).doubleValue(), EPSILON);
    }

    @Test
    void nauticalMileToFootConversionTest() {
        assertEquals(0, FOOT.convertFrom(new BigDecimal(0), NAUTICAL_MILE).doubleValue(), EPSILON);
        assertEquals(6076115.49, FOOT.convertFrom(new BigDecimal(1000), NAUTICAL_MILE).doubleValue(), EPSILON);
    }

}