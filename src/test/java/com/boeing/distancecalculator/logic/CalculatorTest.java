package com.boeing.distancecalculator.logic;

import com.boeing.distancecalculator.model.Distance;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.boeing.distancecalculator.model.LengthUnit.*;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    private final Calculator calculator = new Calculator();

    @Test
    void addWithConsistentUnitTest() {
        Distance result = calculator.add(new Distance(new BigDecimal(100), METER), new Distance(new BigDecimal(200), METER), METER);
        assertEquals(300, result.getValue().doubleValue());
        assertEquals(METER, result.getUnit());
    }

    @Test
    void addWithDifferentUnitsTest() {
        Distance result = calculator.add(new Distance(new BigDecimal(100), METER), new Distance(new BigDecimal(200), FOOT), NAUTICAL_MILE);
        assertEquals(0.09, result.getValue().doubleValue());
        assertEquals(NAUTICAL_MILE, result.getUnit());
    }

    @Test
    void subtractWithConsistentUnitTest() {
        Distance result = calculator.subtract(new Distance(new BigDecimal(300), METER), new Distance(new BigDecimal(200), METER), METER);
        assertEquals(100, result.getValue().doubleValue());
        assertEquals(METER, result.getUnit());
    }

    @Test
    void subtractWithDifferentUnitsTest() {
        Distance result = calculator.subtract(new Distance(new BigDecimal(100), METER), new Distance(new BigDecimal(200), FOOT), NAUTICAL_MILE);
        assertEquals(0.02, result.getValue().doubleValue());
        assertEquals(NAUTICAL_MILE, result.getUnit());
    }

    @Test
    void divideWithConsistentUnitTest() {
        Distance result = calculator.divide(new Distance(new BigDecimal(100), METER), new Distance(new BigDecimal(200), METER), METER);
        assertEquals(0.5, result.getValue().doubleValue());
        assertEquals(METER, result.getUnit());
    }

    @Test
    void divideWithDifferentUnitsTest() {
        Distance result = calculator.divide(new Distance(new BigDecimal(100), METER), new Distance(new BigDecimal(200), FOOT), NAUTICAL_MILE);
        assertEquals(1.64, result.getValue().doubleValue());
        assertEquals(NAUTICAL_MILE, result.getUnit());
    }

    @Test
    void multiplyWithConsistentUnitTest() {
        Distance result = calculator.multiply(new Distance(new BigDecimal(100), METER), new Distance(new BigDecimal(200), METER), METER);
        assertEquals(20000, result.getValue().doubleValue());
        assertEquals(METER, result.getUnit());
    }

    @Test
    void multiplyWithDifferentUnitsTest() {
        Distance result = calculator.multiply(new Distance(new BigDecimal(100), METER), new Distance(new BigDecimal(200), FOOT), NAUTICAL_MILE);
        assertEquals(0.0, result.getValue().doubleValue());
        assertEquals(NAUTICAL_MILE, result.getUnit());
    }
}